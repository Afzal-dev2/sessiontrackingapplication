package com.afzal;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/CounterServlet")
public class CounterServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        // Get the session object
        HttpSession session = request.getSession();
        
        // Get the existing count from the session, or set it to 0 if it doesn't exist
        Integer count = (Integer) session.getAttribute("count");
        if (count == null) {
            count = 0;
        }
        
        // Get the cookies associated with the request
        Cookie[] cookies = request.getCookies();
        boolean cookieExists = false;
        
        // Check if a cookie named "visitCount" exists
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("visitCount")) {
                    // If the cookie exists, update the count
                    count = Integer.parseInt(cookie.getValue());
                    cookieExists = true;
                    break;
                }
            }
        }
        
        // Increment the count
        count++;
        
        // Update the session attribute and cookie
        session.setAttribute("count", count);
        Cookie visitCountCookie = new Cookie("visitCount", String.valueOf(count));
        visitCountCookie.setMaxAge(24 * 60 * 60); // 1 day expiration time
        response.addCookie(visitCountCookie);
        
        out.println("<html>");
        out.println("<head><title>Session and Cookie Example</title></head>");
        out.println("<body>");
        out.println("<h2>Number of times you have visited: " + count + "</h2>");
        out.println("</body>");
        out.println("</html>");
    }
}