# Session Tracking Application

This is a servlet-based application that tracks the number of times a client accesses a webpage and displays the visit count dynamically. This count is tracked using session and cookies on the servlet-side.

## Table of Contents

- [Session Tracking Application](#session-tracking-application)
  - [Prerequisites](#prerequisites)
  - [Usage Steps](#usage-steps)
  - [Overview - Frontend (HTML, CSS, JavaScript)](#overview---frontend-html-css-javascript)
    - [HTML (index.html)](#html-indexhtml)
    - [Internal CSS](#internal-css)
    - [Internal JavaScript](#internal-javascript)
  - [Overview - Servlet (Java)](#overview---servlet-java)
    - [CounterServlet.java](#counterservletjava)
  - [Screenshots](#screenshots)

## Prerequisites

To run this application, you need the following:

1. Eclipse IDE with Tomcat server installed.
2. Java Development Kit (JDK) installed.

## Usage Steps

1. Open Eclipse IDE.
2. Import the project into Eclipse by selecting "File" > "Import" > "Existing Projects into Workspace" and browse to the location of the project.
3. Run the project in Tomcat server.
4. Open a web browser and enter the URL: http://localhost:8080/SessionTrackingApplication/
5. The webpage will display the visit count and a button to refresh the count.
6. Click the button to refresh the visit count.

## Overview - Frontend (HTML, CSS, JavaScript)

### HTML (index.html)

1. Defines the structure of the webpage.
2. Contains a div to display the visit count and a button to refresh the count.

### Internal CSS

Styles the HTML elements for better presentation.

### Internal JavaScript

1. Fetches the visit count from the servlet using JavaScript's Fetch API.
2. Listens for button clicks to refresh the visit count.

## Overview - Servlet (Java)

### CounterServlet.java

1. Defines a servlet class named CounterServlet.
2. In doGet() method, it tracks the number of times a client requests the server using session and cookies.
3. Retrieves the visit count from the session and updates it based on the presence of a cookie.
4. Increments the visit count, updates the session attribute and cookie, and sends an HTML response displaying the visit count.

## Screenshots

![Screenshot1](/screenshots/screenshot1.png)
![Screenshot2](/screenshots/screenshot2.png)
